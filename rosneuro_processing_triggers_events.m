clearvars; clc;

rootdir = '/mnt/data/Research/rosneuro_evaluation/eventtest/20190318/';
savedir = './analysis/eventtest/20190318/';

util_mkdir(pwd, savedir);

[files, nfiles] = util_getfile(rootdir, '.gdf', 'eventtest');

biosemiTrigChannel = 89;
gtecTrigChannel    = 17;
eegoTrigChannel    = 65;

for fId = 1:nfiles 
    util_bdisp(['[io] + Loading file ' num2str(fId) '/' num2str(nfiles)]);
    disp(['     |-File: ' files{fId}]);
    
    cfilename  = files{fId};
    [~, cname] = fileparts(cfilename);
    
    cfields = regexp(cname, '\.', 'split');
    
    cdevice    = cfields{3};
    csampling  = str2double(cfields{4});
    cframerate = str2double(cfields{5});
    
    switch(cdevice) 
        case 'biosemi'
            [cpos, ctrig] = biosemi_extract_triggers_events(cfilename, biosemiTrigChannel);
        case 'gtec'
            [cpos, ctrig] = gtec_extract_triggers_events(cfilename, gtecTrigChannel);
        case 'eego'
            [cpos, ctrig] = eego_extract_triggers_events(cfilename, eegoTrigChannel);
        otherwise
            error('chk:dev', 'Unknown device');
    end
           
    info.device        = cdevice;
    info.sampling_rate = csampling;
    info.frame_rate    = cframerate;
    positions          = cpos;
    trigger            = ctrig;
    
    util_bdisp('[proc] + File information: ');
    disp(['      |-device:           ' cdevice]);
    disp(['      |-sampling rate:    ' num2str(csampling)]);
    disp(['      |-frame rate:       ' num2str(cframerate)]);
    disp(['      |-Number of events: ' num2str(length(cpos.evt))]);
    disp(['      |-Number of onsets: ' num2str(length(cpos.trg))]);
    
    if(isequal(length(cpos.evt), length(cpos.trg)) == false)
        warning('chk:evt', 'Different number of events and trigger onsets');
    end
    
    
    sfilename = [savedir '/' cname '.mat'];
    util_bdisp(['[out] - Saving extracted triggers and events in:' sfilename]);
    save(sfilename, 'positions', 'trigger', 'info');
    
end

function [pos, trg] = biosemi_extract_triggers_events(filename, chnum) 
    [trg, hdr] = sload(filename, chnum);
    
    trg = bitand(hex2dec('FF'), trg);
    pos.trg = find(diff(trg) == 1);
    pos.evt = hdr.EVENT.POS;
end

function [pos, trg] = gtec_extract_triggers_events(filename, chnum) 
    [trg, hdr] = sload(filename, chnum);
    
    pos.trg = find(diff(trg) == 1);
    pos.evt = hdr.EVENT.POS;
end

function [pos, trg] = eego_extract_triggers_events(filename, chnum) 
    [trg, hdr] = sload(filename, chnum);
    
    pos.trg = find(diff(trg) > 0.99);
    pos.evt = hdr.EVENT.POS;
end