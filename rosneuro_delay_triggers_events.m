clearvars; clc;

rootdir = './analysis/eventtest/20190318/';
figdir  = './figures/';
[files, nfiles] = util_getfile(rootdir, '.mat', 'eventtest');

DeviceTypes = {'gtec', 'eego','biosemi'};

TrgPos = [];
EvtPos = [];
Delays = [];
AbsDelays = [];
TrgDelays = [];
EvtDelays = [];
DevK   = [];
SrK    = [];
FrK    = [];
DevK_t   = [];
SrK_t    = [];
FrK_t    = [];
DevK_e   = [];
SrK_e    = [];
FrK_e    = [];

for fId = 1:nfiles 
    util_bdisp(['[io] + Loading file ' num2str(fId) '/' num2str(nfiles)]);
    disp(['     |-File: ' files{fId}]);
    
    cfilename  = files{fId};
    
    cdata = load(cfilename);
    
    % Checking for correct number of events and trigger onsets
    NumTrgPos = length(cdata.positions.trg);
    NumEvtPos = length(cdata.positions.evt);
    
    disp(['     |-Number of hardware events: ' num2str(NumTrgPos)]);
    disp(['     |-Number of software events: ' num2str(NumEvtPos)]);
    if(isequal(NumTrgPos, NumEvtPos) == false)
        warning('chk:evt', 'Different number of events and trigger onsets. Skipping this file');
        continue;
    end
    
    % Get info
    cdevId = find(ismember(DeviceTypes, cdata.info.device));
    csr    = cdata.info.sampling_rate;
    cfr    = cdata.info.frame_rate;
    
    % Concatenate trigger and events positions
    TrgPos = cat(1, TrgPos, cdata.positions.trg);
    EvtPos = cat(1, EvtPos, cdata.positions.evt);
    AbsDelays = cat(1, AbsDelays, (cdata.positions.trg - cdata.positions.evt)/csr);
    Delays = cat(1, Delays, (cdata.positions.trg/csr) - (cdata.positions.evt+csr/cfr)/csr);
    
    cevtdelays = diff(cdata.positions.evt/csr);
    ctrgdelays = diff(cdata.positions.trg/csr);
    TrgDelays = cat(1, TrgDelays, ctrgdelays);
    EvtDelays = cat(1, EvtDelays, cevtdelays);
    
    
    % Create labels
    DevK = cat(1, DevK, cdevId*ones(NumEvtPos, 1));
    SrK = cat(1, SrK, csr*ones(NumEvtPos, 1));
    FrK = cat(1, FrK, cfr*ones(NumEvtPos, 1));
    
    DevK_t = cat(1, DevK_t, cdevId*ones(length(ctrgdelays), 1));
    SrK_t = cat(1, SrK_t, csr*ones(length(ctrgdelays), 1));
    FrK_t = cat(1, FrK_t, cfr*ones(length(ctrgdelays), 1));
    
    DevK_e = cat(1, DevK_e, cdevId*ones(length(cevtdelays), 1));
    SrK_e = cat(1, SrK_e, csr*ones(length(cevtdelays), 1));
    FrK_e = cat(1, FrK_e, cfr*ones(length(cevtdelays), 1));
end
Delays = AbsDelays;
DeviceId     = unique(DevK);
SampleRateId = unique(SrK);
FrameRateId  = unique(FrK);
NumDevices = length(DeviceId);

%% Averaging

AverageDelays = cell(NumDevices, 2);

for dId = 1:NumDevices
    cindex = DevK == DeviceId(dId);
    csamplerates = unique(SrK(cindex));
    cframerates  = unique(FrK(cindex));
    
    Means = nan(length(csamplerates), length(cframerates));
    Stds  = nan(length(csamplerates), length(cframerates));
    
    for srId = 1:length(csamplerates)
        for frId = 1:length(cframerates)
            ccindex = DevK == DeviceId(dId) & SrK == csamplerates(srId) & FrK == cframerates(frId);
            Means(srId, frId) = mean(Delays(ccindex));
            Stds(srId, frId)  = std(Delays(ccindex));
        end
    end
    
    AverageDelays{dId, 1} = Means;
    AverageDelays{dId, 2} = Stds;
    
end
    
    
    

%% Plotting

fig1 = figure;
fig_set_position(fig1, 'Top');

fig1_handles = zeros(NumDevices, 1);
for dId = 1:NumDevices
    subplot(1, NumDevices, dId); 
    cindex = DevK == DeviceId(dId);
    boxplot(Delays(cindex), {SrK(cindex) FrK(cindex)}, 'factorseparator', 1);
    
    xlabel('SampleRate/FrameRate');
    ylabel('Delay [s]');
    
%     ax=gca;
%     ax.YTickLabel=cellstr(num2str(ax.YTick'));
    
    fig1_handles(dId) = gca;
    title(DeviceTypes{DeviceId(dId)});
    
    grid on;
    
end

plot_set_limits(fig1_handles, 'y', [-0.002 0.0625]);
suptitle('Synchrony between triggers and events');

figfilename = [figdir 'rosneuro_events_synchrony.pdf'];
util_bdisp(['Saving figure in ' figfilename]);
fig_export(fig1, figfilename, '-pdf');

%%
fig2 = figure;
fig_set_position(fig2, 'Top');

fig2_handles = zeros(2*NumDevices, 1);
for dId = 1:NumDevices
    subplot(2, NumDevices, dId); 
    cindex_t = DevK_t == DeviceId(dId);
    boxplot(TrgDelays(cindex_t), {SrK_t(cindex_t) FrK_t(cindex_t)}, 'factorseparator', 1);
    title(['Trigger ' DeviceTypes{DeviceId(dId)}]);
    xlabel('SampleRate/FrameRate');
    ylabel('Delay [s]');
    fig2_handles(dId) = gca;
    
    subplot(2, NumDevices, dId+NumDevices); 
    cindex_e = DevK_e == DeviceId(dId);
    boxplot(EvtDelays(cindex_e), {SrK_e(cindex_e) FrK_e(cindex_e)}, 'factorseparator', 1);
    title(['Event ' DeviceTypes{DeviceId(dId)}]);
    
    xlabel('SampleRate/FrameRate');
    ylabel('Delay [s]');
    
    fig2_handles(dId+NumDevices) = gca;
    
end

plot_set_limits(fig2_handles, 'y', 'minmax');
suptitle('Triggers and events delays');

